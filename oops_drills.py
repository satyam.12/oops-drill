class CountDown:
    value = 0

    def incr(self):
        """
        method to increment the value by 1
        """
        self.value += 1
        return self.value

    def decr(self):
        """
        method to decrement the value by 1
        """
        self.value -= 1
        return self.value

    def incrby(self, num):
        """
        method to increment the value by entered number
        """
        self.value += num
        return self.value

    def decrby(self, num):
        """
        method to decrement the value by entered number
        """
        self.value -= num
        return self.value


class Triangle:

    def add_point(self, points):
        """
        method to add points of triangle
        """
        self.points = points

    def perimeter(self):
        """
        method to calcuate the perimeter of triangle
        """
        sides = []
        for point in range(3):
            if point == 0:
                side = (((self.points[point + 1][0] - self.points[point][0]) **
                         2 + (self.points[point + 1][1] - self.points[point][1])**2)**.5)
                sides.append(round(side, 2))
            elif point == 1:
                side = (((self.points[point + 1][0] - self.points[point][0]) **
                         2 + (self.points[point + 1][1] - self.points[point][1])**2)**.5)
                sides.append(round(side, 2))
            else:
                side = (((self.points[point][0] - self.points[point - 2][0]) **
                         2 + (self.points[point][1] - self.points[point - 2][1])**2)**.5)
                sides.append(round(side, 2))
        return round(sum(sides), 2)
    # def is_equal(self,another):
    def __eq__(self, another):  #Changing name from is_equal to __eq__
        """
        method to compare the 2 instances of class Triangle
        """
        return self.points == another.points


t1 = Triangle()
t1.add_point([(0, 0), (0, 3), (4, 0)])
print(t1.perimeter())
t2 = Triangle()
t2.add_point([(1, 2), (2, 1), (1, 5)])
print(t2.perimeter())
#print(t1.is_equal(t2))             #Commenting out is_equal cz function is renamed
t3 = Triangle()
t3.add_point([(1, 2), (2, 1), (1, 5)])
print(t1 == t3)
#print(t1.is_equal(t3))
# print(t3.is_equal(t1))
t2 == t3